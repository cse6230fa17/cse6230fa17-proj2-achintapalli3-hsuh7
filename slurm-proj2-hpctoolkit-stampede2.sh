#!/bin/sh
#SBATCH  -J proj2                        # Job name
#SBATCH  -p development                  # Queue (development or normal)
#SBATCH  -N 4                            # Number of nodes
#SBATCH --tasks-per-node 64              # Number of tasks per node
#SBATCH  -t 00:06:00                     # Time limit hrs:min:sec
#SBATCH  -A TG-TRA170035                 # Our allocation
#SBATCH -o proj2-%j.out                 # Standard output and error log


git rev-parse HEAD

git diff-files

module load hpctoolkit

make test_proj2

pwd; hostname; date

ibrun tacc_affinity hpcrun -t test_proj2 80 100000000 64 0 6

date
